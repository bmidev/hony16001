<div class="modal fade" id="approveModal" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Confirm Approve</h4>
			</div>
			<div class="modal-body">
				<p>Approve this Entry?</p>
			</div>
			<div class="modal-footer">
				<button type="button" id="confirmApprove" class="btn btn-danger">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>