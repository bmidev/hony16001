<div class="reg-contest">
    <h3>*Please fill out all required fields</h3>
    <div ng-controller="register" class="form-container">
        <form name="register" id="register-form" ng-submit="submitForm({{ $challenge_id }})">
            <div>
                <input name="first_name" type="text" id="txtFname" placeholder="First name*" ng-pattern="validate.name" ng-model="user.first_name" ng-class="errors.first_name ? 'error-box' : ''">
                <span ng-bind="errorText.first_name"></span>
            </div>
            <div>
                <input name="last_name" type="text" id="txtLname" placeholder="Last name*" ng-pattern="validate.name" ng-model="user.last_name" ng-class="errors.last_name ? 'error-box' : ''">
                <span ng-bind="errorText.last_name"></span>
            </div>
			
            <div>
                <input name="email" type="text" id="txtEmail" placeholder="Email address*" ng-pattern="validate.email" ng-model="user.email" ng-class="errors.email ? 'error-box' : ''">
                <span ng-bind="errorText.email"></span>
            </div>
            <div>
                <input name="url" type="text" id="txtUrl" placeholder="YouTube link of your record attempt*" ng-model="user.url" ng-class="errors.url ? 'error-box' : '' ">
                <span ng-bind="errorText.url"></span>
            </div>
            <div class="approval clear">
                <input name="terms_and_conditions" type="checkbox" id="terms" class="css-checkbox" ng-model="user.terms_and_conditions">
                <label for="terms" class="css-label" ng-class="errors.terms_and_conditions ? errorBox : ''" ng-click="errorBox = ''">I have read and agree to the terms and conditions.</label>
                <span ng-bind="errorText.terms_and_conditions"></span>
            </div>
            <div class="approval clear">
                <input name="optin" type="checkbox" id="optin" class="css-checkbox" ng-model="user.optin">
                <label for="optin" class="css-label">Sign me up to receive email updates from Doritos<sup>&reg;</sup> and PepsiCo/Frito-Lay<sup>&reg;</sup>.</label>
            </div>
            <div class="error"></div>
            <div><input type="submit" value="Submit" /></div>
        </form>
    </div>
</div>