@extends('layouts.master')
@section('content')
<section class="home-banner">
	<div class="copy-area">
    	<p>Get &amp; Give with Muck Boots | <a href="#prizes" class="show-popup prizes-link">See Prizes</a></p>
        <h1><span class="highlight">Enter today for a chance to win My Little Pony<sup>&reg;</sup> or Transformers<sup>&reg;</sup> Muck Boots and a prize pack for you, and a friend 
of your choice!</span></h1>
		<div class="link clear"><span>Get Boots | </span> <a href="http://www.muckbootcompany.com"><img src="{{ asset('images/flag-us.png') }}" alt=""/></a> <a href="http://www.muckbootcompany.co.uk"><img src="{{ asset('images/flag-uk.png') }}" alt=""/></a></div>
    </div>
	<div class="bg">
        <div class="banner non-mobile">
            <div class="home"><img src="{{ asset('images/home-banner.jpg') }}" alt=""/></div>
            <div class="prizes"><img src="{{ asset('images/home-banner-prizes.jpg') }}" alt=""/></div>
        </div>
        <div class="banner mobile">
            <div class="home"><img src="{{ asset('images/home-mobile-banner.jpg') }}" alt=""/></div>
            <div class="prizes"><img src="{{ asset('images/home-mobile-prizes.jpg') }}" alt=""/></div>
        </div>
    	<div class="little-pony"><img src="{{ asset('images/img-my-little-pony.png') }}" alt=""/></div>
    </div>
</section>
<section class="form-steps">
    <ol>
        <li>Purchase Boots</li>
        <li>Upload Receipt</li>
        <li>You're Entered to Win!</li>
    </ol>
</section>
<section class="form-container" id='main'>
	<div class="page-form" ng-controller="myCtrl">
    	<h3>Thanks For Entering!</h3>
        <p>We'll let you know if you're a winner!</p>
        <figure>
        	<img src="{{ asset('images/img-thankyou.png') }}" alt=""/>
        </figure>
    </div>
</section>
@stop
