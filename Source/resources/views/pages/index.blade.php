@extends('layouts.master')
@section('content')
<section class="home-banner">
	<div class="copy-area">
    	<p>Get &amp; Give with Muck Boots | <a href="#prizes" class="show-popup prizes-link">See Prizes</a></p>
        <h1><span class="highlight">Enter today for a chance to win My Little Pony<sup>&reg;</sup> or Transformers<sup>&reg;</sup> Muck Boots and a prize pack for you, and a friend
of your choice!</span></h1>
		<div class="link clear"><span>Get Boots | </span> <a href="http://www.muckbootcompany.com"><img src="{{ asset('images/flag-us.png') }}" alt=""/></a> <a href="http://www.muckbootcompany.co.uk"><img src="{{ asset('images/flag-uk.png') }}" alt=""/></a></div>
    </div>
	<div class="bg">
        <div class="banner non-mobile">
            <div class="home"><img src="{{ asset('images/home-banner.jpg') }}" alt=""/></div>
            <div class="prizes"><img src="{{ asset('images/home-banner-prizes.jpg') }}" alt=""/></div>
        </div>
        <div class="banner mobile">
            <div class="home"><img src="{{ asset('images/home-mobile-banner.jpg') }}" alt=""/></div>
            <div class="prizes"><img src="{{ asset('images/home-mobile-prizes.jpg') }}" alt=""/></div>
        </div>
    	<div class="little-pony"><img src="{{ asset('images/img-my-little-pony.png') }}" alt=""/></div>
    </div>
</section>
<section class="form-steps">
    <ol>
        <li>Purchase Boots</li>
        <li>Upload Receipt</li>
        <li>You're Entered to Win!</li>
    </ol>
</section>
<section class="form-container">
	<div class="page-form" ng-controller="myCtrl">
    	<h3>Get Started</h3>
        <p>Enter your email to sign up/log in.</p>
		<form method="post" ng-submit='submit()'>
			<div class="element center">
				{!! Form::text('email', old('email'), ['id'=>'txtEmail', 'class' => ($errors->has('email'))? 'error' : '', 'placeholder' => 'Email Address', 'ng-class' => '{error: errors.email}', 'ng-change' => "errors.email = ''", 'ng-model' => 'user.email']) !!}
			</div>
			
			<div ng-show="personalDetails">
				<div class="clear">
					<div class="element left">
						{!! Form::text('first_name', old('first_name'), ['id'=>'txtFirstName', 'class' => ($errors->has('first_name'))? 'error' : '', 'placeholder' => 'First Name', 'ng-class' => '{error: errors.first_name}', 'ng-change' => "errors.first_name = ''", 'ng-model' => 'user.first_name']) !!}	
					</div>
					<div class="element right">
						{!! Form::text('last_name', old('last_name'), ['id'=>'txtLastName', 'class' => ($errors->has('last_name'))? 'error' : '', 'placeholder' => 'Last Name', 'ng-class' => '{error: errors.last_name}', 'ng-change' => "errors.last_name = ''", 'ng-model' => 'user.last_name']) !!}
					</div>
				</div>
			</div>
            <div ng-show="common">
                <div class="upload">
                    <h4>Upload Receipt</h4>
                    <p>Upload your receipt to confirm your entry. Receipt should <br>
                    be an image file (.jpg, .png etc.)</p>
                    <div class="element center">
                        {!! Form::file('receipt', ['id' => 'uploadBtn', 'class' => ($errors->has('receipt'))? 'error inputfile' : 'inputfile', 'ng-class' => '{error: errors.receipt}', 'ng-model' => 'user.receipt', 'file-model' => 'myFile']) !!}
                        <label for="uploadBtn"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> 
                        <span>Browse</span></label>
                    </div>
                </div>
                <div class="tnc">
                    <input id="optin1" type="checkbox" name="tnc" ng-model="user.tnc" class="css-checkbox">
                    <label for="optin1" class="css-label" id="tncID" ng-class = '{error: errors.tnc}' ng-click="removeClass()">I agree to the <a href="#rules" class="show-popup">Terms &amp; Conditions</a></label>
                </div>
                <div class="tnc">
                    <input name="muck_boot_optin" type="checkbox" class="css-checkbox" id='optin2' value="true" ng-model="user.muck_boot_optin">
                    <label for="optin2" class="css-label">I would like to receive information from The Muck Boot Company!</label>
                </div>
            </div>
			<div class="error-msg"> 
            	<span ng-show="errors.tnc"><% errors.tnc[0] %>, </span> 
                <span ng-show="errors.email"><% errors.email[0] %>, </span> 
                <span ng-show="errors.first_name"><% errors.first_name[0] %>,</span> 
            	<span ng-show="errors.last_name"><% errors.last_name[0] %>, </span> 
                <span ng-show="errors.receipt"><% errors.receipt[0] %> </span>
            </div>
			<div class="buttons element center">
				<input name="btnSubmit" type="submit" id="btnSubmit" value="Submit">
			</div>
		</form>
    </div>
</section>
@stop
