@extends('layouts.master')
@section('content')
<section class="home-banner">
	<div class="copy-area">
    	<p>Get &amp; Give with Muck Boots </p>
        <h1>
            <span class="highlight">Sorry, this promotion has ended. Don't worry, though, the boots are still available.<br>
			Visit <a href="http://www.muckbootcompany.com/" target="_blank">www.muckbootcompany.com</a> to get your pair today! </span>
        </h1>
		<div class="link clear"><span>Get Boots | </span> <a href="http://www.muckbootcompany.com"><img src="{{ asset('images/flag-us.png') }}" alt=""/></a> <a href="http://www.muckbootcompany.co.uk"><img src="{{ asset('images/flag-uk.png') }}" alt=""/></a></div>
    </div>
	<div class="bg">
        <div class="banner non-mobile">
            <div class="home"><img src="{{ asset('images/home-banner.jpg') }}" alt=""/></div>
            <div class="prizes"><img src="{{ asset('images/home-banner-prizes.jpg') }}" alt=""/></div>
        </div>
        <div class="banner mobile">
            <div class="home"><img src="{{ asset('images/home-mobile-banner.jpg') }}" alt=""/></div>
            <div class="prizes"><img src="{{ asset('images/home-mobile-prizes.jpg') }}" alt=""/></div>
        </div>
    	<div class="little-pony"><img src="{{ asset('images/img-my-little-pony.png') }}" alt=""/></div>
    </div>
</section>

@stop


@section('scripts')
@stop
