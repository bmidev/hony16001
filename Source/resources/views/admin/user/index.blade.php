@extends('layouts.admin')

@section('content')
    <div id="user-list" class="media-posts panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-list"></i>
            <strong>
                &nbsp; User Listing
            </strong>
        </div>
        <br>
        <div class="container row">
            <div class="col-sm-12">
                <table id="userListing" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="col-md-1">#</th>
                            <th class="col-md-1">First Name</th>
							<th class="col-md-1">Last Name</th>
                            <th class="col-md-2">Email</th>
							<th class="col-md-2">Muck Boot Optin</th>
							<th class="col-md-2">Hasbro Optin</th>
                            <th class="col-md-1 notForPrint">&nbsp;</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('stylesheet')
    {!! HTML::style('https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css') !!}
@stop

@section('scripts')
    {!! HTML::script('https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js') !!}
    {!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') !!}
    {!! HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') !!}
    {!! HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') !!}
    {!! HTML::script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js') !!}
@stop