@extends('layouts.admin')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-file"></i>
            <strong>
                &nbsp; User Details
            </strong>
            <div class="pull-right">
                <a href="{{ URL::to('/admin/user')}}">
                    <button type="button" class="btn btn-info btn-xs"> 
                        <i class="glyphicon glyphicon-list"></i>
                        Listing
                    </button>
                </a>
            </div>
        </div>
        <div class="form-group">
            {{-- ROW::START --}}
            <div class="form-group">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-4">
                                {!! Form::label('name', 'Name:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! $response->first_name . ' ' . $response->last_name !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-4">
                                {!! Form::label('email', 'Email:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! $response->email !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            <hr>
                <div class="well">
                    <a href="{{ URL::to('/admin/user')}}">
                        <button type="button" class="btn btn-info btn-md"> 
                            <i class="glyphicon glyphicon-list"></i>
                            Listing
                        </button>
                    </a>
                </div>
            </div>
    </div>
@stop