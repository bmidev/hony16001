@extends('layouts.admin')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-file"></i>
            <strong>
                &nbsp; {{ $response->user->name }} Details
            </strong>
            <div class="pull-right">
                <a href="{{ URL::to('/admin/entry')}}">
                    <button type="button" class="btn btn-info btn-xs"> 
                        <i class="glyphicon glyphicon-list"></i>
                        Listing
                    </button>
                </a>
            </div>
        </div>
        <div class="form-group">
            {{-- ROW::START --}}
            <div class="form-group">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-4">
                                {!! Form::label('user', 'User Name:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! HTML::link('/admin/user/' . $response->user->id, $response->user->first_name . ' ' . $response->user->last_name) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            {{-- ROW::START --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-4">
                                {!! Form::label('status', 'Status:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('status', ucfirst($response->status), ['class' => 'form-label']) !!}
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="col-md-4">
                                {!! Form::label('image', 'Picture:', ['class' => 'form-label']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
			{{-- ROW::START --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
						<div class="col-md-12">
							{!! HTML::image($response->receipt, '', ['class' => 'img-responsive', 'id' => 'receiptImage']) !!}
						</div>
					</div>
                </div>
            </div>
            {{-- ROW::END --}}
            <hr>
            <div class="well">
                <a href="{{ URL::to('/admin/entry')}}">
                    <button type="button" class="btn btn-info btn-md"> 
                        <i class="glyphicon glyphicon-list"></i>
                        Listing
                    </button>
                </a>
				{{--<button type="button" class="btn btn-warning btn-md" id="rotate"> 
					<i class="glyphicon glyphicon-retweet"></i>
					Rotate
				</button>--}}
            </div>
        </div>
    </div>
@stop

@section('scripts')
{{-- <script src='https://raw.githubusercontent.com/wilq32/jqueryrotate/master/jQueryRotate.js'></script>
<script>
$(function(){
	var angle = 0;
	$('#rotate').on('click', function(){
		angle += 90;
		$('#receiptImage').rotate(angle);
	});
});
</script> --}}
@stop