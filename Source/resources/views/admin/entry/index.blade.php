@extends('layouts.admin')

@section('content')
    <div id="metric-list" class="media-posts panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-list"></i>
            <strong>
                &nbsp; Entry Listing
            </strong>
        </div>
        <br>
        <div class="container row">
            <div class="col-sm-12">
                <table id="entryListing" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="col-md-1">Status</th>
                            <th class="col-md-1">#</th>
                            <th class="col-md-2">User</th>
                            <th class="col-md-2">Receipt</th>
							<th class="col-md-2">Created At</th>
                            <th class="col-md-1 notForPrint">&nbsp;</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
	{!! Form::hidden('user_id', $user_id, ['id' => 'userID']) !!}
    {{-- [START] Approval Modal --}}
    @include('partials.approve')
    {{-- [END] Approval Modal --}}
    
    {{-- [START] Rejection Modal --}}
    @include('partials.reject')
    {{-- [END] Rejection Modal --}}
@stop

@section('stylesheet')
    {!! HTML::style('https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css') !!}
@stop

@section('scripts')
    {!! HTML::script('https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js') !!}
    {!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') !!}
    {!! HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') !!}
    {!! HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') !!}
    {!! HTML::script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js') !!}
    
    <script>
    $(function(){
		var loadMoreRequest = null;
		var entryID = null;
        $('body').on('mouseover','.markApprove',function(){
            $('.markApprove').on('click', function(){
                entryID = this.id;
                $('#confirmApprove').on('click', function(){
                    if (loadMoreRequest !== null) {
						return;
					}
                    loadMoreRequest = $.ajax({
                        url: APP.adminUrl + 'entry/' + entryID + '/mark-approve',
                        type: 'POST',
						asyn: false,
                        success: function (data) {
							if($('#entryListing span#' + entryID).hasClass('label-danger')){
								$('#entryListing span#' + entryID).removeClass('label-danger').addClass('label-success');
							} else {
								$('#entryListing span#' + entryID).removeClass('label-default').addClass('label-success');
							}
							$('#entryListing span#' + entryID).text('Approved');
							$('#approveModal').modal('hide');
							loadMoreRequest = null;
                        },
						error: function() {
							loadMoreRequest = null;
						}
                    });
                });
            });
        }).on('mouseout','.markApprove',function(){
            $('.markApprove').unbind('click');
        });
        
        $('body').on('mouseover','.markReject',function(){
            $('.markReject').on('click', function(){
                entryID = this.id;
                $('#confirmReject').on('click', function(){
					if (loadMoreRequest !== null) {
						return;
					}
                    loadMoreRequest = $.ajax({
                        url: APP.adminUrl + 'entry/' + entryID + '/mark-reject',
                        type: 'POST',
						asyn: false,
                        success: function (data) {
							if($('#entryListing span#' + entryID).hasClass('label-success')){
								$('#entryListing span#' + entryID).removeClass('label-success').addClass('label-danger');
							} else {
								$('#entryListing span#' + entryID).removeClass('label-default').addClass('label-danger');
							}
							$('#entryListing span#' + entryID).text('Rejected');
							$('#rejectModal').modal('hide');
							loadMoreRequest = null;
                        },
						error: function() {
							loadMoreRequest = null;
						}
                    });
                });
            });
        }).on('mouseout','.markReject',function(){
            $('.markReject').unbind('click');
        });
    });
    </script>
@stop