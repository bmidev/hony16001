<script type="text/javascript">
	google.load('visualization', '1', {packages: ['geochart']});

    function drawVisualization()
    {
		var data = new google.visualization.DataTable();

		data.addColumn('string', 'State');
		data.addColumn('number', 'Visits');
		data.addRows(<?php echo $data->getCount(); ?>);

		<?php foreach($data->getData() as $key => $row) : ?>
			data.setValue(<?php echo $key; ?>, 0, '<?php echo $row['state']; ?>');
			data.setValue(<?php echo $key; ?>, 1, <?php echo $row['visits']; ?>);
		<?php endforeach; ?>

		var geochart = new google.visualization.GeoChart(document.getElementById('<?php echo $data->getChartId(TRUE); ?>'));

		geochart.draw(data, {
						width: '100%',
						height: <?php echo $data->getHeight(); ?>,
						region: "<?php echo $data->getOptions()['country_code']; ?>",
						resolution: "provinces"
					 });
    }

    google.setOnLoadCallback(drawVisualization);

    window.onresize = function(event) {
    	drawVisualization();
	}
</script>
<div id="<?php echo $data->getChartId(TRUE); ?>" class="state_map"></div>