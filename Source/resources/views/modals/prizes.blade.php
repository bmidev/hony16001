<section class="popup-area" id="prizes">
    <div class="prize-area">
        <h3>You and a friend will each win a Hasbro prize pack and Muck Boots!</h3>
        <figure><img src="{{ asset('images/img-popup-prizes.jpg') }}" alt=""></figure>		
    </div>
    <div class="footer">Select your Hasbro prize pack preference upon entry. <br>
    For full prize details see the <a href="#rules" class="show-popup cboxElement">Terms &amp; Conditions</a>.</div>
    <div class="external-img right">
        <img src="{{ asset('images/popup-right.png') }}" alt="">
    </div>
	<div class="external-img left">
        <img src="{{ asset('images/popup-left.png') }}" alt="">
    </div>
</section>
