<!DOCTYPE html>
<html lang="en" ng-app="siteApp">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Honeywell</title>

<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

<link href="{{ asset('css/vendor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/site.css') }}" rel="stylesheet" type="text/css"/>

<script src="{{ asset('js/modernizr.custom.js') }}"></script>
</head>

<body class="bg-promo">
	@yield('content')
<div class="hide">
</div>

@if (app()->environment() === 'production') 
<!--GA code -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', '', 'auto');
    ga('send', 'pageview');

</script>
<!--GA code end-->
@endif
<div id="fb-root"></div>
<!-- Scripts -->
<script>
    var APP = APP || {};
    APP.baseUrl = "{{ BASE_URL }}";
</script> 

@yield('scripts')
</body>
</html>
