<!DOCTYPE html>
<html lang="en" ng-app="siteApp">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Muck® Boots Hasbro Sweepstakes</title>

<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

<link href="{{ asset('css/vendor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/site.css') }}" rel="stylesheet" type="text/css"/>

<script src="{{ asset('js/modernizr.custom.js') }}"></script>
</head>

<body>
<div class="main-container">
    <header id="home">
    	<div class="logo"><a href="{{BASE_URL}}"><img src="{{ asset('images/logo.jpg') }}" alt=""/></a></div>
    </header>

    @yield('content')
    <footer class="clear {{$class}}">
        <div class="links">
       	<a href="#home" class="goto">Home</a> |  <a href="#rules" class="show-popup">Terms &amp; Conditions</a> |  <a href="http://www.muckboots.com/privacy-policy/" target="_blank">Privacy Policy</a> |  <a href="http://brandmovers.com/?utm_source=Muck-Boots&utm_medium=powered-by&utm_term=hony16001&utm_content=Footer-link&utm_campaign=Muck-Boots" target="_blank">Powered by Brandmovers</a> |  <a href="http://www.muckbootcompany.com/" target="_blank">muckbootcompany.com</a></div>
    </footer>
</div>
<div class="hide">
    @include('modals.prizes')
    @include('modals.rules')
</div>

@if (app()->environment() === 'production') 
<!--GA code -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-87403428-1', 'auto');
    ga('send', 'pageview');

</script>
<!--GA code end-->
@endif
<div id="fb-root"></div>
<!-- Scripts -->
<script>
    var APP = APP || {};
    APP.baseUrl = "{{ BASE_URL }}";
</script> 

<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/site.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@yield('scripts')
</body>
</html>
