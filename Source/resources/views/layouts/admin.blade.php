<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Honeywell | Admin | Brandmovers</title>
    <link rel="icon" type="image/png" href="{{ BASE_URL }}favicon.ico" />
    <link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet">
    
    <!-- Datatable Stylesheet -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
    
    <!-- Fonts -->
    <style>
        a {
            text-decoration: none !important;
        }
    </style>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
    <script>
        var BASE_URL = "{{ BASE_URL }}";
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//cdn.jsdelivr.net/momentjs/2.11.2/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <style>html{visibility:hidden;}</style><script>if(self==top){document.documentElement.style.visibility='visible';}else{top.location=self.location;}</script> 
</head>
<body>
    @if (!Auth::guest())
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <span class="navbar-brand">
                        Honeywell
                    </span>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{!! url('admin/dashboard') !!}">Dashboard</a></li>
                        <li><a href="{{ url('admin/user') }}">User</a></li>
                        <li><a href="{{ url('admin/entry') }}">Entry</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ BASE_URL }}admin/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    @endif
	
    <div class="container">
        @yield('content')
    </div>

    <!-- Stylesheet -->
    @yield('stylesheet')
    <!-- Stylesheet -->

    <script>
        var APP = APP || {};
        APP.adminUrl = "<?php echo BASE_URL . 'admin/'; ?>";
    </script>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.1/Chart.min.js"></script>
	<script type="text/javascript" src="{{ asset('js/admin/admin.js') }}"></script>
    
	<!-- DataTables -->
	<script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
	@yield('scripts')
</body>
</html>
