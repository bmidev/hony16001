var APP = APP || {};
var myApp = angular.module('siteApp', [], function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

myApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

myApp.controller('myCtrl', ['$scope', '$http', '$window', function($scope, $http, $window){
    $scope.personalDetails = false;
    $scope.common = false;
    $scope.all = 'no';
    $scope.action = 'show';
    
    var loadMoreRequest = null;
    
    $scope.removeClass = function(){
        $('#tncID').removeClass('error');
    };
    
    $scope.submit = function(){
        if (loadMoreRequest !== null) {
                return;
        }
        if($scope.all == 'yes' || $scope.action == 'save'){
            $('#btnSubmit').val('Uploading...');
        }
        
        var userData = $scope.user;
        var file = $scope.myFile; 
        var uploadUrl = APP.baseUrl;
        
        var fd = new FormData();
        
        if($scope.personalDetails == true){ // [New User] Send Personal Details
            fd.append('first_name', (userData)?userData.first_name: '');
            fd.append('last_name', (userData)?userData.last_name: '');
        }
        
        fd.append('email', (userData)?userData.email: '');
        
        if($scope.common == true){ // Appending Upload Feature & Checkbox [TNC & Optin] to Form Data
            var muckBootOptin = 0;
            var hasbroOptin = 0;

            if(userData){
                if(userData.muck_boot_optin == undefined){
                    muckBootOptin = 0;
                } else if (userData.muck_boot_optin == true){
                    muckBootOptin = 1;
                } else {
                    muckBootOptin = 0;
                }

                if(userData.hasbro_optin == undefined){
                    hasbroOptin = 0;
                } else if (userData.hasbro_optin == true){
                    hasbroOptin = 1;
                } else {
                    hasbroOptin = 0;
                }
            }
            
            fd.append('muck_boot_optin', muckBootOptin);
            fd.append('hasbro_optin', hasbroOptin);
            fd.append('tnc', (userData)?userData.tnc: '');
            fd.append('receipt', file);
        }
        
        fd.append('all', $scope.all);
        fd.append('action', $scope.action);
        
        loadMoreRequest = $http.post(
            uploadUrl,
            fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }
        ).success(function(data){
            if(data.new_entry && data.action == 'show'){ // [New] User
                $scope.personalDetails = true; // Show First Name and Last Name
                $scope.common = true; // Show Upload Feature and Checkboxes
                $scope.all = 'yes'; // Validates all Entry
            } else if (data.new_entry == false && data.action == 'show'){ // [Returning] User
                $scope.common = true; // Show Upload Feature and Checkboxes
                $scope.all = 'part'; // Allow Part Validation on Entry
                $scope.action = 'save'; // Add Entry for Existing User
            } else if(data.status) { // [200] Status: true
                $window.location.href = APP.baseUrl + 'thanks#main';
            } else { // Custom Validation Error
                $scope.errors = {email: [data.message]};
            }
            loadMoreRequest = null;
        }).error(function(response, status){
            if (status === 422) { // Validation Error
                $scope.errors = response;
                $('#btnSubmit').val('Submit');
            }
            loadMoreRequest = null;
        });
    };
}]);

$(function(){
//    $('#uploadBtn').on('change', function(){
//        setTimeout(function(){
//            document.getElementById('uploadFile').value = uploadFile.value.replace("C:\\fakepath\\", "")
//        }, 50);
//    });
});