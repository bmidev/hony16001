var APP = APP || {};

APP.headerHomeOffset = 150;
APP.header = $('header');
APP.width = $(window).width();
APP.height = $(window).height();

APP.updateHeaderOffset = function () {
    APP.headerOffset = APP.header.innerHeight();
    if (APP.headerOffset > 100 && !APP.header.hasClass('sticky')) {
        APP.headerOffset -= 50;
    }	
};

APP.colorboxResize = function(){
	setTimeout(	function(){
			$.colorbox.resize();
		}, 200);    
}

$(document).ready(function () {
	
    $(document).on('click', 'nav.main-menu li a, .goto', function (event) {
        var $this = $(this);
        $('nav.main-menu li').removeClass('active');
        $this.parent().addClass('active');

        var id = $this.attr("href");
        var offset = id === '#home' ? APP.headerHomeOffset : APP.headerOffset;
        var target = $(id).offset().top - offset;
        $('html, body').animate({
            scrollTop: target
        }, 1000);
        event.preventDefault();
    });
    	
    $('.show-popup').colorbox({
        inline: true, width: '90%', maxWidth: '800', opacity: 0.85, onComplete: function () {
            parent.$.colorbox.resize();
        }
    });

	$('.close').on('click', function(){
		$.colorbox.close();	
	});

	/*$('.prizes-link').on('click', function(e){
		e.preventDefault();
		$('.banner').css('display', 'none');
		$('.banner.prizes').css('display', 'block');
		$(this).hide();	
		$('.home-link').show();	
	});
	
	$('.home-link').on('click', function(e){
		e.preventDefault();
		$('.banner').css('display', 'none');
		$('.banner.home').css('display', 'block');
		$(this).hide();	
		$('.prizes-link').show();	
	});*/
	
	$('.home-banner .banner').slick({
        infinite: true,
		autoplay: true,
        speed: 300,
        arrows: true,
		dots: true,
		responsive: [
		{
		  breakpoint: 640,
		  settings: {
			arrows: false,
			dots: false
		  }
		}
	  ]
    });
});


$(window).scroll(function () {
    var windscroll = $(window).scrollTop();

    if (windscroll > 30) {
        $('header .logo').addClass('sm');

    } else {
        $('header .logo').removeClass('sm');
    }
});
