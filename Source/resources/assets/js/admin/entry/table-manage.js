$(function() {
    var buttonCommon = {
        exportOptions: {
            columns: ':not(.notForPrint)'
        }
    };
    
    var userID = $('#userID').val();
    
    $('#entryListing').DataTable({
        processing: true,
        serverSide: false,
        pageLength: 25,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        order: [[1, 'desc']],
        dom: 'Blfrtip',
        ajax: {
            url: APP.adminUrl + "datatable/entryProcessing?user_id=" + userID,
            method:'POST'
        },
        columns: [{
                data: 'status',
                render: function(data, type, row){
                    var str = '';
                    if(data == 'pending'){
                        str += '<span class="label label-default" id="' + row['id'] + '">Pending</span>';
                    } else if (data == 'rejected'){
                        str += '<span class="label label-danger" id="' + row['id'] + '">Rejected</span>';
                    } else {
                        str += '<span class="label label-success" id="' + row['id'] + '">Approved</span>';
                    }
                    
                    return str;
                }
            },{
                data: 'id'
            },{
                data: 'name',
                render: function(data, type, row){
                    var str =
                            '<a href="' + APP.adminUrl + 'user/' + row['user_id'] +'">' +
                                data +
                            '</a>';
                    
                    return str;
                }
            },{
                data: 'receipt',
                render: function(data, type, row){
                    var str = 
                        '<a href="' + data +'" target="_blank">' +
                            '<img src="' + data +'" height=100px width=100px/>' + 
                        '</a><span style="display:none">' + data + '</span>';
                    
                    return str;
                }
            },{
                data: 'date',
                render: function(data, type, row){
                    var rawData = data.split(' ');
                    var date = rawData[0].split('-');
                    
                    return date[1] + '-' + date[2] + '-' + date[0] + ' ' + rawData[1];
                }
            },{
                sortable: false,
                data: 'id',
                render: function(data, type, row){
                    var str = '';
                    
                    var approve = '<button type="button" class="btn btn-success btn-sm markApprove" id="' + data + '"data-toggle="modal" data-target="#approveModal">' +
                                        '<i class="glyphicon glyphicon-star"></i>' +
                                        '&nbsp;Approve' +
                                    '</button><br>';
                    
                    var reject = '<button type="button" class="btn btn-danger btn-sm markReject" id="' + data + '"data-toggle="modal" data-target="#rejectModal">' +
                                    '<i class="glyphicon glyphicon-remove"></i>' +
                                    '&nbsp;&nbsp;Reject&nbsp;' +
                                '</button><br>';
                    
                    str += approve + reject;
                    
                    str += '<a href="' + APP.adminUrl + 'entry/' + data +'">' +
                                '<button type="button" class="btn btn-info btn-sm">' +
                                    '<i class="glyphicon glyphicon-info-sign"></i>' +
                                    '&nbsp;&nbsp; Details' + 
                                '</button>' +
                            '</a>';
                    
                    return str;
                }
            }
        ],
        buttons: [
            $.extend( true, {}, buttonCommon, {
                extend: 'copyHtml5'
            } ),
            $.extend( true, {}, buttonCommon, {
                extend: 'excelHtml5'
            } ),
            $.extend( true, {}, buttonCommon, {
                extend: 'pdfHtml5'
            } )
        ]
    });
});