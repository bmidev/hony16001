$(function() {
    var buttonCommon = {
        exportOptions: {
            columns: ':not(.notForPrint)'
        }
    };
    
    $('#userListing').DataTable({
        processing: true,
        serverSide: false,
        dom: 'Bfrtip',
        ajax: {
            url: APP.adminUrl + "datatable/userProcessing",
            method:'POST'
        },
        columns: [{
                data: 'id'
            },{
                data: 'first_name'
            },{
                data: 'last_name'
            },{
                data: 'email'
            },{
                data: 'muck_boot_optin',
                render: function(data, type, row){
                    var str = '';
                    if(data == 0){
                        str += '<span class="label label-danger" id="' + row['id'] + '">No</span>';
                    } else {
                        str += '<span class="label label-success" id="' + row['id'] + '">Yes</span>';
                    }
                    
                    return str;
                }
            },{
                data: 'hasbro_optin',
                render: function(data, type, row){
                    var str = '';
                    if(data == 0){
                        str += '<span class="label label-danger" id="' + row['id'] + '">No</span>';
                    } else {
                        str += '<span class="label label-success" id="' + row['id'] + '">Yes</span>';
                    }
                    
                    return str;
                }
            },{
                data: 'id',
                render: function(data, type, row){
                    var str = '';
                    str += '<a href="' + APP.adminUrl + 'user/' + data +'/entry">' +
                                '<button type="button" class="btn btn-info btn-sm">' +
                                    '<i class="glyphicon glyphicon-info-sign"></i>' +
                                    '&nbsp;&nbsp; Entries' + 
                                '</button>' +
                            '</a>';
                    
                    return str;
                }
            }
        ],
        buttons: [
            $.extend( true, {}, buttonCommon, {
                extend: 'copyHtml5'
            } ),
            $.extend( true, {}, buttonCommon, {
                extend: 'excelHtml5'
            } ),
            $.extend( true, {}, buttonCommon, {
                extend: 'pdfHtml5'
            } )
        ]
    });
});