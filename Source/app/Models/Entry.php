<?php namespace App\Models;

class Entry extends PlatformBaseModel{
    protected $fillable = ['user_id', 'receipt', 'hit_id', 'status_update_method', 'is_manual_intervention_needed', 'status'];
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];
    protected $table = 'entries';
    
    /* Relationship Methods */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    /* Relationship Methods */
}
