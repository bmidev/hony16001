<?php namespace App\Models;

class User extends PlatformBaseModel {
    protected $fillable = ['first_name', 'last_name', 'email', 'muck_boot_optin', 'hasbro_optin'];
    protected $table = 'users';
}
