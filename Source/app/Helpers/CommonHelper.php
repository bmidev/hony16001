<?php

function getIp()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function change_date_format($date, $new_format = 'Y-m-d'){
//    dd($original_format, $date);
//    dd(strtotime($date));
    return date($new_format, strtotime($date));
}

function get_youtube_video_id($url){
    $rx = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
    $has_match = preg_match($rx, $url, $matches);
    if($has_match == 1 && strlen($matches[7]) == 11){
        return $matches[7];
    }
    return null;
}