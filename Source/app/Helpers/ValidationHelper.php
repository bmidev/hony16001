<?php

function isValidAge($date, $min_age_limit, $max_age_limit = 100, $format = 'm/d/Y')
{
    //date in mm/dd/yyyy format; or it can be in other formats as well
    $birthDate = $date;
    //explode the date to get month, day and year
    $birthDate = explode("/", $birthDate);
    //get age from date or birthdate
    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
        ? ((date("Y") - $birthDate[2]) - 1)
        : (date("Y") - $birthDate[2]));

    if($age > $min_age_limit && $age <= $max_age_limit){
        return true;
    }

    return false;
}

function is_valid_youtube_link($url){
    $rx = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
    $has_match = preg_match($rx, $url, $matches);
    if($has_match == 1 && strlen($matches[7]) == 11){
        $theURL = "http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=$matches[7]&format=json";
        $headers = get_headers($theURL);
        if (substr($headers[0], 9, 3) !== "404") {
            return true;
        }
    }
    return false;
}

function get_days_from_last_date($date_time){
    $date = new DateTime($date_time);
    $now = new DateTime();
    $diff = $now->diff($date);
    return $diff->days;
}