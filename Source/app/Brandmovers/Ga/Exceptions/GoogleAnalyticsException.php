<?php namespace Brandmovers\Ga\Exceptions;



class GoogleAnalyticsException extends \Exception
{
    public function __construct()
    {
        throw new \Exception('
        1. FOR NEW GOOGLE AUTH 2
            - Create project "https://console.developers.google.com/project"
            - Make a Service Account inside that project
            - Paste the new generated email in (config/admin.php) file  on \'ga_client_email\'
            - Generate a new p12 key file and paste in a config/ga-key-file/
        2. ENABLE GOOGLE ANALYTICS API
            - In the sidebar on the left, expand APIs & auth.
            - Next, click APIs. Select the Enabled APIs link in the API section to see a list of all your enabled APIs.
            - Make sure that the Google Analytics API is on the list of enabled APIs.
            - If you have not enabled it, select the API from the list of APIs, then select the Enable API button for the API.
        3. PROVIDE PERMISSION TO THE NEW EMAIL
            - Goto the google analytics page and provide permission to this email(e.g.: XXXXX@developer.gserviceaccount.com) for the respective project
        4. CONFIG FOLDER
            - Provide the necessary details in the config/admin.php file. Refer dashboard/config/config.php in the package.
     ');
    }

}