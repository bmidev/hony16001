<?php namespace App\Http\Controllers;

use App\Http\Requests\Validator\Web\RegisterValidationRequest;
use App\Libs\Platform\Storage\User\UserRepository;
use App\Libs\Platform\Storage\Entry\EntryRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Image;

class SiteController extends Controller {
    private $entry;
	private $user;
    
    public function __construct(EntryRepository $entry, UserRepository $user){
        parent::__construct();
        
		$this->entry = $entry;
        $this->user = $user;
    }
    
	/**
	 * Project Homepage
	 * 
	 * @return type
	 */
    public function index(){
		if(env('APP_ENV') == 'production'){
			if($this->current_time < strtotime(env('APP_START_DATE', '2016-11-10'))){
				return $this->beforePromo();
			} else if($this->current_time >= strtotime(env('APP_START_DATE', '2016-11-10')) && $this->current_time <= strtotime(env('APP_END_DATE', '2016-12-16'))){
				return view('pages.' . __FUNCTION__)->with(['class' => '']);
			} else {
				return $this->promoOver();
			}
		} else {
			return view('pages.' . __FUNCTION__)->with(['class' => '']);
		}
    }
    
	/**
	 * Method to allow User Registeration
	 * 
	 * @param RegisterValidationRequest $registerValidator
	 * @return type
	 */
    public function register(RegisterValidationRequest $registerValidator) {
		/* Checking Email already Exist */
		$userEmail = Input::get('email');
		$emailExist = $this->user->getUserFromEmail($userEmail);
		
		if(count($emailExist) > 0 && Input::get('action') == 'save'){ // [Existing User: Save]
			/* Validating 1 User Entry Per Day */
			$validEntry = $this->isEntryExistForSameUserSameDay($userEmail);
			
			if($validEntry === TRUE){
				return [
					'status' => false,
					'message' => 'You\'re done for this day, try again tomorrow'
				];
			}
			/* Validating 1 User Entry Per Day */
			
			$userReceipt = Input::file('receipt');
			
			/* Declaring Variable */
			$entryData = [];
			$entryData['user_id'] = $emailExist->id;
			$entryData['receipt'] = $this->saveToS3($userReceipt);
			$entryData['status'] = 'pending';
			/* Declaring Variable */
			
			/* Adding Receipt for Existing User */
			$entryResponse = $this->entry->create($entryData);
			/* Adding Receipt for Existing User */

			/* Building Response */
			if($entryResponse){
				$this->ajax_response = [
					'new_entry' => false,
					'status' => true,
					'message' => 'Success'
				];
			}
			/* Building Response */

			return $this->ajax_response;
		} else if(count($emailExist) > 0 && Input::get('action') == 'show'){ // [Existing User: Show]
			/* Validating 1 User Entry Per Day*/
			$validEntry = $this->isEntryExistForSameUserSameDay($userEmail);

			if($validEntry === TRUE){
				return [
					'status' => false,
					'message' => 'You\'re done for this day, try again tomorrow'
				];
			}
			/* Validating 1 User Entry Per Day*/
			
			/* Adding Parameter to View Upload Feature */
			$this->ajax_response = [
				'new_entry' => false,
				'action' => 'show'
			];
			
			return $this->ajax_response;
			/* Adding Parameter to View Upload Feature */
		} else if(count($emailExist) == 0 && Input::get('all') == 'no'){ // [New User: Show]
			$this->ajax_response = [
                'new_entry' => true,
				'action' => 'show'
            ];
			
			return $this->ajax_response;
		}
		/* Checking Email already Exist */
		
		// [New User: Save]
        /* Default Variable */
        $userData = Input::only('first_name', 'last_name', 'email', 'muck_boot_optin', 'hasbro_optin');
        $userReceipt = Input::file('receipt');
        
        $entryData = [];
        $entryData['status'] = 'pending';
        /* Default Variable */
        
		/* Validating 1 User Entry Per Day*/
		$validEntry = $this->isEntryExistForSameUserSameDay($userData['email']);

		if($validEntry === TRUE){
			return [
				'status' => false,
				'message' => 'You\'re done for this day, try again tomorrow'
			];
		}
		/* Validating 1 User Entry Per Day*/
		
		/* Uploading receipt to S3 */
		$entryData['receipt'] = $this->saveToS3($userReceipt);
        /* Uploading receipt to S3 */
		
		/* Getting User Details */
        $userResponse = $this->user->getUserFromEmail($userData['email']);
        /* Getting User Details */
        
        /* Checking if User Exists (If not then create new) */
        if(count($userResponse) > 0){
            $entryData['user_id'] = $userResponse->id;
        } else {
            $userResponse = $this->user->create($userData);
            $entryData['user_id'] = $userResponse->id;
        }
        /* Checking if User Exists (If not then create new) */
		
		/* Adding Receipt for New/Existing User */
		$entryResponse = $this->entry->create($entryData);
		/* Adding Receipt for New/Existing User */
		
		/* Building Response */
        if($entryResponse){
            $this->ajax_response = [
				'new_entry' => false,
                'status' => true,
                'message' => 'Success'
            ];
        }
        /* Building Response */
		
        return $this->ajax_response;
    }
    
	/**
	 * Method to upload User Receipt to S3
	 * 
	 * @param type $userReceipt
	 * @return type
	 */
    public function saveToS3($userReceipt) {
		$fileNameComplete = $userReceipt->getClientOriginalName();
		$fileName = explode('.', $fileNameComplete)[0];
        $userReceiptName = $fileName . '_' . time() . '.' . $userReceipt->getClientOriginalExtension();
        
//		$userReceipt = Image::make($userReceipt)->orientate();
//		$userReceipt = $userReceipt->stream();
//		
//        $s3 = Storage::disk('s3');
//        $s3FileName = config('app.project_id') . '/' . $userReceiptName;
//        $s3->put($s3FileName, $userReceipt->__toString(), 'public');
		
		$s3 = Storage::disk('s3');
        $s3FileName = config('app.project_id') . '/' . $userReceiptName;
        $s3->put($s3FileName, file_get_contents($userReceipt), 'public');
		
        return Storage::disk('s3')->url($s3FileName);
    }
    
	/**
	 * Method for showing before Promo Screen
	 * 
	 * @return type
	 */
	public function beforePromo() {
		return view('pages.blank')->with(['class' => '']);;
	}
	
	/**
	 * Method for showing after Promo Ends
	 * 
	 * @return type
	 */
    public function promoOver() {
        return view('pages.promo-over')->with(['class' => 'footer-promo-over']);
    }
	
	/**
	 * Method to acknowledge user.
	 * 
	 * @return type
	 */
	public function thanks() {
        return view('pages.thanks')->with(['class' => '']);
    }
	
	public function isEntryExistForSameUserSameDay($email){
        $userID = DB::table('users')->where('email', $email)->select('id')->first();
        
        if(!is_null($userID)){
            $entry_date_raw = DB::table('entries')
                    ->select('created_at')
                    ->where('user_id',  $userID->id)
                    ->orderBy('created_at', 'desc')
                    ->first();
            
            $entry_date = count($entry_date_raw) ? ($entry_date_raw->created_at): null;
			
			if(is_null($entry_date)){
				return false;
			}
			
			$entryDate = explode(' ', $entry_date)[0];
            $tomorrow = Carbon::parse($entryDate)->addDay()->format('Y-m-d');
			
			$next_valid_date = Carbon::parse($tomorrow);
			$latest_post_date = Carbon::parse($entryDate);
			
			return Carbon::now()->between($latest_post_date, $next_valid_date);
        } else {
            return false;
        }
    }
}
