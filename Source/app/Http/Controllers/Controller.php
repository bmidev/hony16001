<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use URL;

class Controller extends BaseController{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected $ajax_response;
    protected $current_time = '';
	
    public function __construct(){
		$this->current_time = time();
        define('BASE_URL', URL::to('/').'/');
        
        $this->ajax_response = [
            'status' => 'error',
            'message' => 'An error occurred, please try again later'
        ];
    }
}
