<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libs\Platform\Storage\User\UserRepository;

class UserController extends Controller {
    private $user;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user){
        parent::__construct();
        
        $this->user = $user;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        /* HTML View Response */
        return view('admin.user.' . __FUNCTION__);
        /* HTML View Response */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(){
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        /* Default Variables */
        $fields = [];
        $with = [];
        /* Default Variables */
        
        /* Get User */
        $userResponse = $this->user->view($id, $fields, $with);
        /* Get User & Options */
        
        /* HTML View Response */
        return view('admin.user.' . __FUNCTION__)->with(['response' => $userResponse]);
        /* HTML View Response */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        /* Redirect */
        return redirect()->back();
        /* Redirect */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id){
        /* Redirect */
        return redirect()->back();
        /* Redirect */
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        return redirect()->back();
    }
    
	/**
	 * Method to add data to datatable
	 * 
	 * @return type
	 */
    public function datatables() {
        /* Query Creation & Fire */
        return $this->user->datatables();
        /* Query Creation & Fire */
    }
}
