<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App;

use Illuminate\Support\Facades\Config;
use Brandmovers\Charts\LineChart;
use Brandmovers\Charts\PieChart;
use Brandmovers\Charts\StateMap;
use Brandmovers\Charts\Stats;
use Brandmovers\Charts\WorldMap;
use Brandmovers\Ga\Model\GaModel;


class DashboardController extends Controller {

	/*
	|---------------------------------------------------------------------------
	| Admin Dashboard Controller
	|---------------------------------------------------------------------------
	|
	| Primary admin controller displaying dashboard stats
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        parent::__construct();
    }

	/**
	 * Admin dashboard
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request){
        $app_start_date = date('m/d/Y', Config::get('admin.app_start_date'));

        if(Config::get('admin.app_end_date') > time()){
            $app_end_date = date('m/d/Y');
        } else {
            $app_end_date = date('m/d/Y', Config::get('admin.app_end_date'));
        }

        $startDate = $request->input('start-date', $app_start_date);
        $endDate = $request->input('end-date', $app_end_date);

        $startDate = date_create_from_format('m/d/Y', $startDate);
        $endDate = date_create_from_format('m/d/Y', $endDate);

        $this->gaModel = new GaModel(Config::get('admin.ga_profile_id'));
        
        // Line Chart of Daily Visitors
        $options = array('chartId' => 'visitors', 'width' => '960');

        $start_date = $startDate->getTimeStamp();
        $end_date = $endDate->getTimeStamp();

        $this->db_analytics($start_date, $end_date);

        $visitors = new LineChart($this->gaModel->dailyVisits($start_date, $end_date), $options, $start_date, $end_date);

        // Basic Stats from Google Analytics
        $basic = new Stats($this->gaModel->basicStats($start_date, $end_date));

        $colors = array('#5B5C77', '#D9D676', '#90D669', '#8EAC4F', '#C98956', '#1C82D8');

        // Pie Charts
        $options_mobile = array('width' => 280, 'height' => 180, 'chart_id' => 'mobile_pie_chart', 'colors' => $colors);
        $options_browsers = array('width' => 280, 'height' => 180, 'chartId' => 'browser_pie_chart', 'colors' => $colors);
        $options_mediums = array('width' => 280, 'height' => 180, 'chartId' => 'medium_pie_chart', 'colors' => $colors);
        $options_channels = array('width' => 280, 'height' => 180, 'chartId' => 'channel_pie_chart', 'colors' => $colors);
        $mobile = new PieChart($this->gaModel->mobileVisitors($start_date, $end_date, 5), $options_mobile);
        $browsers = new PieChart($this->gaModel->getVisitorsByBrowsers($start_date, $end_date, 5), $options_browsers);
        $mediums = new PieChart($this->gaModel->getVisitorsByMedium($start_date, $end_date, 5), $options_mediums);
        $channels = new PieChart($this->gaModel->getVisitorsByChannel($start_date, $end_date, 5), $options_channels);

        // Default Country State Maps
        $states = new StateMap($this->gaModel->getVisitorsFromCountry(Config::get('admin.dash_country_filter'), $start_date, $end_date));
        $countries = new WorldMap($this->gaModel->getVisitorsByCountry($start_date, $end_date));
        $gaEvents = $this->gaModel->getEvents($start_date, $end_date);
//        $fbShares = isset($gaEvents['share']['share facebook']['Main Share FB']) ? $gaEvents['share']['share facebook']['Main Share FB'] : 0;
//        $twShares = isset($gaEvents['share']['share twitter']['Main Share TW']) ? $gaEvents['share']['share twitter']['Main Share TW'] : 0;
//        $emailShares = isset($gaEvents['share']['share email']['Email Share']) ? $gaEvents['share']['share email']['Email Share'] : 0;
        
        return view('admin.dashboard.index', [
            'filters' => [
		'start-date' => $request->input('start-date', $app_start_date),
		'end-date' => $request->input('end-date', $app_end_date) // Use Request Data since we actually add 1 to end date since we're checking for less than
            ],
            'visitors' => $visitors,
            'basic' => $basic,
            'mobile' => $mobile,
            'mediums' => $mediums,
            'channels' => $channels,
            'browsers' => $browsers,
            'states' => $states,
            'countries' => $countries,
//            'fbShares' => $fbShares,
//            'twShares' => $twShares,
//            'emailShares' => $emailShares,
	]);
    }

    private function db_analytics($start_date, $end_date){
        $start_time = date('Y-m-d 00:00:00', $start_date);
        $end_time = date('Y-m-d 23:59:59', $end_date);
//        dd($this->data['unique_entries']);
    }

}
