<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libs\Platform\Storage\Entry\EntryRepository;
use App\Http\Requests\Validator\Admin\EntryValidationRequest;
use Illuminate\Support\Facades\Input;

class EntryController extends Controller {
    private $entry;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EntryRepository $entry){
        parent::__construct();
        
        $this->entry = $entry;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($user_id=null){
		/* HTML View Response */
        return view('admin.entry.' . __FUNCTION__)->with(['user_id' => $user_id]);
        /* HTML View Response */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(){
        return redirect()->back();
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        /* Default Variables */
        $fields = ['*'];
        $with = [
            'user' => ['id', 'first_name', 'last_name']
        ];
        /* Default Variables */
        
        /* Get Entry */
        $response = $this->entry->view($id, $fields, $with);
        /* Get Entry & Options */
        
        /* HTML View Response */
        return view('admin.entry.' . __FUNCTION__)->with(['response' => $response]);
        /* HTML View Response */
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        return redirect()->back();
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(EntryValidationRequest $entryValidator, $id){
        /* Separation & Limitations of Data By Models */
        $data = Input::only('status');
        /* Separation & Limitations of Data By Models */
        
        /* Query Creation & Fire */
        $mr = $this->entry->update($id, $data);
        /* Query Creation & Fire */
        
        /* Redirect Based on Model Response */
        if ($mr) { // If Successful
            return redirect('/admin/entry/' . $id)->with(['message' => 'Entry Updated']);
        }
        /* Redirect Based on Model Response */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        /* Query Creation & Fire */
        $this->entry->delete($id);
        /* Query Creation & Fire */

        /* Redirect Based on Model Response */
        return redirect('/admin/entry')->with(['message' => 'User Deleted!']);
        /* Redirect Based on Model Response */
    }
    
    /**
     * Method to Mark entry approved
     * 
     * @param type $entry_id
     */
    public function markApprove($entry_id) {
        /* Separation & Limitations of Data By Models */
        $data['status'] = 'approved';
        /* Separation & Limitations of Data By Models */
        
        /* Query Creation & Fire */
        $mr = $this->entry->update($entry_id, $data);
        /* Query Creation & Fire */
        
        /* Redirect Based on Model Response */
        if ($mr) { // If Successful
            return response(['status' => true, 'message' => 'Entry Approved', 'data' => []]);
        } else {
            return response(['status' => false, 'message' => 'Oops! Something went wrong', 'data' => []]);
        }
        /* Redirect Based on Model Response */
    }
    
    /**
     * Method to Mark entry rejected
     * 
     * @param type $entry_id
     */
    public function markReject($entry_id) {
        /* Separation & Limitations of Data By Models */
        $data['status'] = 'rejected';
        /* Separation & Limitations of Data By Models */
        
        /* Query Creation & Fire */
        $mr = $this->entry->update($entry_id, $data);
        /* Query Creation & Fire */
        
        /* Redirect Based on Model Response */
        if ($mr) { // If Successful
            return response(['status' => true, 'message' => 'Entry Rejected', 'data' => []]);
        } else {
            return response(['status' => false, 'message' => 'Oops! Something went wrong', 'data' => []]);
        }
        /* Redirect Based on Model Response */
    }
	
	/**
	 * Method to add data to datatable
	 * 
	 * @return type
	 */
    public function datatables() {
        /* Query Creation & Fire */
        return $this->entry->datatables();
        /* Query Creation & Fire */
    }
}
