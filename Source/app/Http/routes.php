<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'SiteController@index');
    Route::post('/', 'SiteController@register');
	Route::get('thanks', 'SiteController@thanks');
    Route::get('gallery', 'SiteController@gallery');
    Route::get('promo-over', 'SiteController@promoOver');
	
    /* [START] Admin Routes */
    Route::group(['namespace' => 'Admin'], function() {
        Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {
            Route::get('dashboard', 'DashboardController@index');
            
			Route::resource('entry', 'EntryController');
            Route::resource('user', 'UserController');
            
            Route::post('entry/{entry_id}/mark-approve', 'EntryController@markApprove')->where(['entry_id' => '[0-9]+']);
            Route::post('entry/{entry_id}/mark-reject', 'EntryController@markReject')->where(['entry_id' => '[0-9]+']);
            
			Route::get('user/{user_id}/entry', 'EntryController@index')->where(['entry_id' => '[0-9]+']);
			
			Route::post('datatable/entryProcessing', 'EntryController@datatables');
            Route::post('datatable/userProcessing', 'UserController@datatables');
        });
        
        Route::controller('admin', 'AuthController');
    });
    /* [END] Admin Routes */
});

