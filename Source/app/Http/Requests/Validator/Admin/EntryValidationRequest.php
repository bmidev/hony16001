<?php namespace App\Http\Requests\Validator\Admin;

use App\Http\Requests\Request;

class EntryValidationRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'status' => 'Required'
        ];
    }

    public function messages(){
        return [
            'status.required' => 'Status is required'
        ];
    }
}
