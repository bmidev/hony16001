<?php namespace App\Http\Requests\Validator\Web;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Input;

class RegisterValidationRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
		if(Input::get('all') == 'yes'){
			$rules = [
				'first_name' => 'Required|Max:100|not_in:undefined',
				'last_name' => 'Required|Max:100|not_in:undefined',
				'email' => 'Required|email',
				'receipt' => 'Required|image|max:10240',
				'tnc' => 'accepted'
			];
		} else if(Input::get('all') == 'part'){
			$rules = [
				'email' => 'Required|email',
				'receipt' => 'Required|image',
				'tnc' => 'accepted'
			];
		} else {
			$rules = [
				'email' => 'Required|email'
			];
		}
		
		return $rules;
    }
    
    public function messages(){
        return [
            'first_name.required' => 'Please enter your First Name',
			'first_name.not_in' => 'Please enter your First Name',
            'first_name.max' => 'First Name cannot exceed 100 characters',
            'last_name.required' => 'Please enter your Last Name',
			'last_name.not_in' => 'Please enter your Last Name',
            'last_name.max' => 'Last Name cannot exceed 100 characters',
            'email.required' => 'Please enter your Email',
            'email.email' => 'Invalid email',
            'receipt.required' => 'Kindly, Upload Receipt',
            'receipt.image' => 'Invalid Image',
			'tnc.accepted' => 'Kindly accept, Terms and Conditions'
        ];
    }
}
