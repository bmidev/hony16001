<?php namespace App\Libs\Platform\Storage\Entry;

use App\Models\Entry;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

class EloquentEntryRepository implements EntryRepository {
    protected $model;

    /**
     * Contructor method
     * 
     * @param Entry $model
     */
    public function __construct(Entry $model) {
        $this->model = $model;
    }

    /**
     * Method to fetch all the entries from the table
     */
    public function all() {
        return $this->model->all();
    }

    /**
     * Method to create a new entry in the table
     * 
     * @param array $data : array containing the new entry's data
     */
    public function create($data) {
        return $this->model->create($data);
    }
    
    public function datatables() {
        $result = $this->model
                ->join('users', 'entries.user_id', '=', 'users.id')
                ->select([
                    'entries.status',
                    'entries.id',
                    'users.id as user_id',
					DB::raw('CONCAT(users.first_name, " ", users.last_name) AS name'),
                    'entries.receipt',
					'entries.created_at as date',
                ]);
		
        if((int)Input::get('user_id') != 0){
			$result->where('user_id', (int)Input::get('user_id'));
		}
		
        return Datatables::of($result)
                ->make(true);
    }
    
    /**
     * Method to delete an existing entry from the database
     * 
     * @param int $id : id of the entry
     */
    public function delete($id) {
        $resource = $this->find($id);

        return $resource->delete();
    }

    /**
     * Method to fetch and return a particular record from the table by 'id'
     * 
     * @param int $id : id of the entry
     */
    public function find($id) {
        return $this->model->find($id);
    }
    
    /**
     * Get a paginated listing
     * 
     * @param int $limit
     * @param array $fields
     * @param array $filters
     * @param string $sort
     * @param array $with
     * @throws DBException
     */
    public function listing($limit=32, $fields=[], $filters=[], $sort=['id'], $with=[]) {
        $order = 'DESC';	// default query sorting order
        $query = $this->model->newQuery();
        $tableName = $this->model->getTable();
        
        if (!$fields) {
            $fields = [$tableName . '.*'];
        }
        if ($filters) {
            if (isset($filters['id'])) {
                $query->where($tableName . '.id', '=', $filters['id']);
            }
            if (isset($filters['status'])) {
                $query->where($tableName . '.status', 'LIKE', '%' . $filters['status'] . '%');
            }
            if (isset($filters['search'])) {
                $query->where($tableName . '.name', 'LIKE', '%' . $filters['search'] . '%');
            }
        }
        if ($with) {
            $with = $this->model->processWithSelects($with);
        }
        
        try {
            $query->with($with);
            foreach ($sort as $val) {
                $query->orderBy($val, $order);
            }
            
            return $query->paginate($limit, $fields);
        }
        catch (Exception $e) {
            throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }

    /**
     * Method to update an existing entry in the database
     * 
     * @param int $id : id of the entry
     * @param array $data : array containing the entry's updated data
     * @return model_object
     */
    public function update($id, $data) {
        $resource = $this->model->find($id);
        $resource->fill($data);
        $resource->save();

        return $resource;
    }

    /**
     * Method to fetch an entry along with the respective data based on the criteria
     * 
     * @param int $id
     * @param string $fields
     * @param array $with
     * @return model_object
     * @throws DBException
     */
    public function view($id, $fields=[], $with=[]) {
        $query = $this->model->newQuery();
        $tableName = $this->model->getTable();
        
        if (!$fields) {
            $fields = [$tableName . '.*'];
        }
        if ($with) {
            $with = $this->model->processWithSelects($with);
            $query->with($with);
        }
        
        try {
            return $query->with($with)->where($tableName . '.id', '=', $id)->first($fields);
        } catch (Exception $e) {
            throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }
}