<?php namespace App\Libs\Platform\Storage\User;

interface UserRepository {
    public function all();
    
    public function create($data);
    
    public function datatables();
    
    public function find($id);
    
    public function getUserFromEmail($email);
    
    public function listing($limit, $fields, $filters, $sort, $with);
    
    public function view($id, $fields, $with);
}
