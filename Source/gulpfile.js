var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
        .scripts([
                'vendor/bootstrap-datepicker.min.js',
                'vendor/collapse.js',
                'admin/admin.js',
                'admin/*'
            ], 'public/js/admin/admin.js')

        .less([
            'app.less',
            'admin/admin.less'
        ], 'public/css/admin/admin.css')
		
        .styles([
            'vendor/colorbox.css',
			'vendor/slick.css'
        ], 'public/css/vendor.css')
        
        .sass("site.scss")
		
        .scripts([
            'vendor/jquery.js',            
            'vendor/angular/angular.min.js',
			'vendor/colorbox.js',
			'vendor/slick.min.js',
			'vendor/custom-file-input.js'
        ], 'public/js/vendor.js')
		
        .scripts([
            'app/main.js'
        ], 'public/js/app.js')
		
        .scripts('site.js', 'public/js/site.js')
    ;

});
