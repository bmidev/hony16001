<?php
ini_set('date.timezone', 'America/New_York');
!defined('DASH_START_DATE') ? define('DASH_START_DATE', '2016-11-08 00:00:01'): null;
!defined('DASH_END_DATE') ? define('DASH_END_DATE', '2017-05-01 23:59:59'): null;

return [
	'dash_app_name' => 'Brandmovers',
	'dash_country_filter' => 'United States',
	'dash_country_chart' => 'US',
	/*
	  |
	  |--------------------------------------------------------------------------
	  | Google Analytics Information
	  |--------------------------------------------------------------------------
	  |
	  |	Call each of these variables using gaEmail(), gaPassword(), or
	  |	gaProfileId() respectively.
	  |
	  |--------------------------------------------------------------------------
	  | Where to find the Google Analytics profile ID
	  |--------------------------------------------------------------------------
	  |
	  |   To get the ga_profile_id config item, log into http://google.com/analytics
	  |   with the Brandmover's e-mail and password (below), and then click
	  |   into the site for your project. The URL will look something like:
	  |
	  |   https://www.google.com/analytics/web/?hl=en#report/visitors-overview/a30383075w56852320p57967808/
	  |
	  |   The ga_profile_id is the last set of numbers after the 'p' in the url
	  |   (in this example, 57967808).
	  |
	 */
	'ga_email' => '562925543804-e626um5hdmn5q4ertcv1tn3gn1btaqp2@developer.gserviceaccount.com',
	'ga_password' => "MIIGwAIBAzCCBnoGCSqGSIb3DQEHAaCCBmsEggZnMIIGYzCCAygGCSqGSIb3DQEHAaCCAxkEggMVMIIDETCCAw0GCyqGSIb3DQEMCgECoIICsjCCAq4wKAYKKoZIhvcNAQwBAzAaBBSQ7eTSNYXlqbymv5cOawe1+ygQAAICBAAEggKAOheHTE83fhrpnaIfn7Lx7ymbfFd8LaUuZai3GsAK+Ktib5FL8vD8mdIQXZedf/BGLDYJSCZ5my4TC8vKK7NlFZWy/Q8tfRSj+2t2o9o1z72Rv7neqH1oeuoj6Tv9184hMxg708DYanG94G7/BBjtVz0AZQuPIg1ZDsjDvZ3V3w9Oi5U7JlP+Va9HovOePqCdHqkYw3Oz6frUzj8Qy1LNOgTubHyuVNwJC/qEyjlI43+AR2sZSDt+0S7Itybw5T0UMmRxgEdX6Hkww0PckIg3Rk8t+qNs62pZaDvjuo2VMA/a2ONyxrln4kjm5AnuM5z3AuyvRfTbXYWr3u9tI63rq9y0MhcXLdAmqZjUAFt/3XQMl8/G2igb1bWryqejj1XUmXvmz455O/1QGCVQv2yBRwcizsqnDnawKRzZ1c6HwGcmAKXCj6tSppJ/kbzyu9hmJ5rlobSSEJuxlNxEe9YiHXSWFaNYwB3/FT8lwI4WWinxGrJ3U4GOnlLrj8PEOxFk8RKwKo7+leY6j/8JPL19E6HVVhTYB/ghsTPW4jAek4g2PhLmVF+uro0Hu1N/nva7Q7JBDQp6nzz8loHfj9Ek3miIS907h4tmNtp7xXFkxlu7rtkdZq4f770LAjIqhHapdtACI0tCD+IkUcANQO4TQGpej6STEBtrNGJb3QphmyIwVNAc+eDVphw3mICqBMHA2qo3lh6WLxJIYbC/gBAf7RSwrukxnT/L1iIJS+j0RGzlAbx5ibFqNTUn48k7kOZVTmPnS2oNyD8a1c60slnQUogPmGVDWtTD53UdWmZnWXW2GfvcG+xl8FGvb0KC+31uiI0SKpdyKe091yYbzYMRAjFIMCMGCSqGSIb3DQEJFDEWHhQAcAByAGkAdgBhAHQAZQBrAGUAeTAhBgkqhkiG9w0BCRUxFAQSVGltZSAxNDMyNzU2NjI1NDU2MIIDMwYJKoZIhvcNAQcGoIIDJDCCAyACAQAwggMZBgkqhkiG9w0BBwEwKAYKKoZIhvcNAQwBBjAaBBTVdUKXmnzKRJQNBi7lCVV5F9oFCgICBACAggLgQw5xPpKRD/DTWRJFgytoBKpEDDynUyTvQ38ESxApNByYea5eTmXItoHvnCG/39LznbV8SZ3bmqEHr/XxS3bO4eY1FyYQYfoZRS2Ngk2a2Qua/vrw/sHamN2d8bjfuEZ5Zamz6EhQHAteJ+iWi1Smlc1OFFh7eQ1Fid+xGrhVCiZ37Y6IQ7nH9O9BQVRgj2eM8NJSfTOPp4cz2NaCSCIVOqjXB9SMCJWuBCZsnA7E35o2guyQoVAJ/aVU8lZNE7qlFuRvmy3Y8pkgjvOpKdnromtZ/PyNb/CFnVLvBpHFZd6u+foqgDp9YLlPXgZVi4lMMtAnMo19A5Jz/S3hdqrMRSAOeXx5ys+vONtPTs+KzCw55RX6uDHOMRtburlohkd8HCdSa51d5LqTpA38Mguu/wyRtIkpgxeVBRiC6qgB6T+UGBTo2eJx2GwQ7K0jkstRzG0FSYzKl4a4kILSu4ExKsZU2OqS6qUGdpjBRK3164UFhgO6RMomV7s+GuD7RaZ2jLW/9ZinfmTEfx+ABYHqOcJOYOS6skZYi6fvMVj57C6qda8yPWjYiXK+O7oG4Db319gKMQBKvPxPMhX4fyju2CLy7aO6mmVRyqqZGBt+3ECkt2hxLIUAWqtQ6D5eKbS/yIE/gKZLCIFlmlLfLIwyAfzz9gxAdRKCFBfB1YhgBqOhuRuLnEFQ9ELjx+IFrrlA8Jrt1srXOG5JPHfbie94MpOFPQNESEMhJxKFNNXFrz505OTl8H2QSLHxep2ecvBPHOCGsKwAYpGdu1sppBMlWLiGxLVJZ1wy3lXroEIPxd60P1kAiL9PNq6WSrQhTxjFZwO66qEeHiP6hoAgII5Kxp00F/LZzkJhz1paZ5DwVDoQfBn/CtzGAO0kVMVoQm6vrvxvfsl5V3vWJ8e8CK6dFB5wnvt0nShO5IINt1VFFr61yseBF1Oca2isLecbX0khdHxk7r/IdsgMMVppn6P36DA9MCEwCQYFKw4DAhoFAAQUK8bJThbVUcFgnaXQ+YzluhbuSg0EFGpcCXv544akUrGKAs/6I0UBO2yEAgIEAA==",
    'ga_profile_id' => env('APP_ENV') == 'local' ? '122569138' : '133715061',
	/*
	 * Admin title
	 * Displays in page title and header
	 */
	'title' => 'Brandmovers administrator',
	/*
	 * Admin url prefix
	 */
	'prefix' => 'admin',
	/*
	 * Before filters to protect admin from unauthorized users
	 */
	'beforeFilters' => ['admin.auth'],
	/*
	 * Path to admin bootstrap files directory in app directory
	 * Default: 'app/admin'
	 */
	'bootstrapDirectory' => app_path('admin'),
	/*
	 * Path to images directory
	 * Default: 'public/images'
	 */
	'imagesDirectory' => public_path('images'),
	/*
	 * Path to files directory
	 * Default: 'public/files'
	 */
	'filesDirectory' => public_path('files'),
	/*
	 * Path to images upload directory within 'imagesDirectory'
	 * Default: 'uploads'
	 */
	'imagesUploadDirectory' => 'uploads',
	/*
	 * Authentication config
	 */
	'auth' => [
		'model' => '\Brandmovers\AdminAuth\Entities\Administrator',
		'rules' => [
			'username' => 'required',
			'password' => 'required',
		]
	],
	/*
	 * Blade template prefix, default admin::
	 */
	'bladePrefix' => 'admin::',
	/*
	 * Caching on or off, default TRUE
	 */
	'caching' => TRUE,

	'app_start_date' => strtotime(DASH_START_DATE) < time() ? strtotime(DASH_START_DATE) : strtotime('2016-11-08 00:00:00'),
	'app_end_date' => strtotime(DASH_END_DATE),
];
