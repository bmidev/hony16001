<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('admins', function(Blueprint $table){
            $table->increments('id');
            $table->enum('role', ['admin', 'client'])->default('admin')->index();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();
        });

        \DB::table('admins')->insert([
            'role'	=> 'admin',
            'email'   	=> 'admin@brandmovers.com',
            'password'   => Hash::make('h0n3yW3l!'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('admins')->insert([
            'role'	=> 'client',
            'email'   	=> 'client@brandmovers.com',
            'password'   => Hash::make('Hh4QDajE'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::drop('admins');
    }

}
